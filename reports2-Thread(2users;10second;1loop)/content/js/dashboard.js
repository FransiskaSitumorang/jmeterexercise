/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7916666666666666, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "GET SELLER PRODUCT"], "isController": false}, {"data": [1.0, 500, 1500, "GET SELLER PRODUCT ID"], "isController": false}, {"data": [0.5, 500, 1500, "POST AUTH REGISTER"], "isController": false}, {"data": [1.0, 500, 1500, "PUT BUYER ORDER ID"], "isController": false}, {"data": [1.0, 500, 1500, "DELETE SELLER PRODUCT ID"], "isController": false}, {"data": [1.0, 500, 1500, "POST BUYER ORDER"], "isController": false}, {"data": [1.0, 500, 1500, "GET BUYER ORDER"], "isController": false}, {"data": [0.0, 500, 1500, "GET BUYER PRODUCT"], "isController": false}, {"data": [1.0, 500, 1500, "POST AUTH LOGIN"], "isController": false}, {"data": [1.0, 500, 1500, "GET BUYER PRODUCT ID"], "isController": false}, {"data": [1.0, 500, 1500, "GET BUYER ORDER ID"], "isController": false}, {"data": [0.0, 500, 1500, "POST SELLER PRODUCT"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 24, 0, 0.0, 2655.6250000000005, 260, 25341, 277.0, 14102.5, 25330.0, 25341.0, 0.6488766323302783, 317.19342480128154, 3.254416247600508], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["GET SELLER PRODUCT", 2, 0, 0.0, 289.5, 270, 309, 289.5, 309.0, 309.0, 309.0, 0.3755163349605708, 0.26880222024033046, 0.12504987326323697], "isController": false}, {"data": ["GET SELLER PRODUCT ID", 2, 0, 0.0, 273.0, 273, 273, 273.0, 273.0, 273.0, 273.0, 0.37530493525989866, 0.26791787858885346, 0.12717852786639144], "isController": false}, {"data": ["POST AUTH REGISTER", 2, 0, 0.0, 1140.5, 1112, 1169, 1140.5, 1169.0, 1169.0, 1169.0, 0.3272251308900524, 0.18853791721204188, 0.49307458687827227], "isController": false}, {"data": ["PUT BUYER ORDER ID", 2, 0, 0.0, 276.0, 275, 277, 276.0, 277.0, 277.0, 277.0, 0.3726476616359232, 0.24746133780510526, 0.1419263555058692], "isController": false}, {"data": ["DELETE SELLER PRODUCT ID", 2, 0, 0.0, 278.0, 272, 284, 278.0, 284.0, 284.0, 284.0, 0.37453183520599254, 0.11777270599250937, 0.13496313202247193], "isController": false}, {"data": ["POST BUYER ORDER", 2, 0, 0.0, 280.0, 274, 286, 280.0, 286.0, 286.0, 286.0, 0.37209302325581395, 0.2492732558139535, 0.14934593023255813], "isController": false}, {"data": ["GET BUYER ORDER", 2, 0, 0.0, 268.0, 268, 268, 268.0, 268.0, 268.0, 268.0, 0.3733432891543775, 0.44772027254060104, 0.12323245286540974], "isController": false}, {"data": ["GET BUYER PRODUCT", 2, 0, 0.0, 25319.0, 25297, 25341, 25319.0, 25341.0, 25341.0, 25341.0, 0.06576783952647156, 385.24745149621833, 0.021836977967773757], "isController": false}, {"data": ["POST AUTH LOGIN", 2, 0, 0.0, 332.5, 332, 333, 332.5, 333.0, 333.0, 333.0, 0.37885963250615645, 0.18203021405569236, 0.10618429153248722], "isController": false}, {"data": ["GET BUYER PRODUCT ID", 2, 0, 0.0, 268.5, 260, 277, 268.5, 277.0, 277.0, 277.0, 0.3718163227365681, 0.4172040574456219, 0.124180842163971], "isController": false}, {"data": ["GET BUYER ORDER ID", 2, 0, 0.0, 269.5, 268, 271, 269.5, 271.0, 271.0, 271.0, 0.37306472673008767, 0.44665757321395266, 0.12532643163588883], "isController": false}, {"data": ["POST SELLER PRODUCT", 2, 0, 0.0, 2873.0, 2838, 2908, 2873.0, 2908.0, 2908.0, 2908.0, 0.2546148949713558, 0.17206397199236154, 14.067970241884149], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 24, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
