/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7708333333333334, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "GET SELLER PRODUCT"], "isController": false}, {"data": [1.0, 500, 1500, "GET SELLER PRODUCT ID"], "isController": false}, {"data": [0.25, 500, 1500, "POST AUTH REGISTER"], "isController": false}, {"data": [1.0, 500, 1500, "PUT BUYER ORDER ID"], "isController": false}, {"data": [1.0, 500, 1500, "DELETE SELLER PRODUCT ID"], "isController": false}, {"data": [1.0, 500, 1500, "POST BUYER ORDER"], "isController": false}, {"data": [1.0, 500, 1500, "GET BUYER ORDER"], "isController": false}, {"data": [0.0, 500, 1500, "GET BUYER PRODUCT"], "isController": false}, {"data": [1.0, 500, 1500, "POST AUTH LOGIN"], "isController": false}, {"data": [1.0, 500, 1500, "GET BUYER PRODUCT ID"], "isController": false}, {"data": [1.0, 500, 1500, "GET BUYER ORDER ID"], "isController": false}, {"data": [0.0, 500, 1500, "POST SELLER PRODUCT"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 24, 0, 0.0, 2709.7916666666665, 263, 26154, 302.0, 14042.0, 25925.75, 26154.0, 0.6432074612065499, 314.3918297577252, 3.2274745480797575], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["GET SELLER PRODUCT", 2, 0, 0.0, 295.5, 271, 320, 295.5, 320.0, 320.0, 320.0, 0.35205069530012323, 0.2520050387255765, 0.11723563193099806], "isController": false}, {"data": ["GET SELLER PRODUCT ID", 2, 0, 0.0, 277.0, 273, 281, 277.0, 281.0, 281.0, 281.0, 0.35137034434293746, 0.2508317594869993, 0.11906788035839774], "isController": false}, {"data": ["POST AUTH REGISTER", 2, 0, 0.0, 1357.5, 1178, 1537, 1357.5, 1537.0, 1537.0, 1537.0, 0.30599755201958384, 0.17630718329253367, 0.4624328239749082], "isController": false}, {"data": ["PUT BUYER ORDER ID", 2, 0, 0.0, 275.5, 275, 276, 275.5, 276.0, 276.0, 276.0, 0.42462845010615713, 0.28363853503184716, 0.1617237261146497], "isController": false}, {"data": ["DELETE SELLER PRODUCT ID", 2, 0, 0.0, 313.0, 306, 320, 313.0, 320.0, 320.0, 320.0, 0.34934497816593885, 0.10985262008733625, 0.12588700873362446], "isController": false}, {"data": ["POST BUYER ORDER", 2, 0, 0.0, 285.5, 273, 298, 285.5, 298.0, 298.0, 298.0, 0.42408821034775235, 0.28576256361323155, 0.17021509223918574], "isController": false}, {"data": ["GET BUYER ORDER", 2, 0, 0.0, 267.5, 265, 270, 267.5, 270.0, 270.0, 270.0, 0.42716787697565145, 0.5160221326356258, 0.14099877189235369], "isController": false}, {"data": ["GET BUYER PRODUCT", 2, 0, 0.0, 25697.5, 25241, 26154, 25697.5, 26154.0, 26154.0, 26154.0, 0.0652613717940351, 382.2421640466945, 0.02166881485348822], "isController": false}, {"data": ["POST AUTH LOGIN", 2, 0, 0.0, 339.0, 334, 344, 339.0, 344.0, 344.0, 344.0, 0.35081564637782847, 0.1685559550955973, 0.09832430713909841], "isController": false}, {"data": ["GET BUYER PRODUCT ID", 2, 0, 0.0, 288.0, 263, 313, 288.0, 313.0, 313.0, 313.0, 0.42052144659377627, 0.4718546309924306, 0.14044759251471825], "isController": false}, {"data": ["GET BUYER ORDER ID", 2, 0, 0.0, 282.5, 272, 293, 282.5, 293.0, 293.0, 293.0, 0.42489908646696406, 0.5124515349479499, 0.14273953685999577], "isController": false}, {"data": ["POST SELLER PRODUCT", 2, 0, 0.0, 2839.0, 2835, 2843, 2839.0, 2843.0, 2843.0, 2843.0, 0.24378352023403216, 0.16474433203315456, 13.475229308873718], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 24, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
